package com.javapro.tp1.service;


import com.javapro.tp1.dal.ProductApiDAL;
import com.javapro.tp1.dal.ProductDAL;
import com.javapro.tp1.dto.BasketRequestDto;
import com.javapro.tp1.model.NutritionScore;
import com.javapro.tp1.repository.NutritionScoreRepository;
import com.javapro.tp1.repository.RuleRepository;
import org.json.JSONObject;
import com.javapro.tp1.dto.ProductDto;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import javax.print.attribute.standard.MediaSize;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ProductService {

    @Qualifier("ProductApiDAL")
    private final ProductDAL productDAL;

    private final RuleRepository ruleRepository;
    private final NutritionScoreRepository nutritionScoreRepository;

    public ProductDto getProduct(String barCode)
    {
        ProductDto productDto = new ProductDto();


        String jsonData = productDAL.getProductAsJSON(barCode);
        JSONObject jsonObject = new JSONObject(jsonData);


        productDto.setNutritionScore(0);

        int negativeScore = getNegativeScore(jsonObject);
        int positiveScore = getPositiveScore(jsonObject);
        int nutritionScore =  negativeScore - positiveScore;

        Optional<NutritionScore> classment = nutritionScoreRepository.findClasseAndColorByNutritionScore(nutritionScore);

        return ProductDto.builder()
                .id(jsonObject.getJSONObject("product").getString("_id"))
                .barCode(jsonObject.getString("code"))
                .name(jsonObject.getJSONObject("product")
                        .getJSONObject("ecoscore_data")
                        .getJSONObject("agribalyse")
                        .getString("name_fr"))
                .nutritionScore(nutritionScore)
                .classe(classment.get().getClasse())
                .color(classment.get().getColor())
                .build();
    }

    public int getNegativeScore(JSONObject jsonObject) {
        final String ENERGY = "energy_100g";
        final String SATURATED_FAT = "saturated-fat_100g";
        final String SUGARS = "sugars_100g";
        final String SALT = "salt_100g";
        final String NEGATIVE_COMPONENT = "N";


        JSONObject nutriments = jsonObject.getJSONObject("product")
                .getJSONObject("nutriments");
        float energy_100g = nutriments.getFloat("energy_100g");
        float sugars_100g = nutriments.getFloat("sugars_100g");
        float salt_100g = nutriments.getFloat("salt_100g");
        float saturated_fat = jsonObject.getJSONObject("product")
                .getJSONObject("nutriscore_data").getFloat("saturated_fat");

        float[] extracted_scores = {energy_100g, saturated_fat, sugars_100g, salt_100g };
        String[] names = {ENERGY, SATURATED_FAT, SUGARS, SALT };
        int score = 0;

        for(int i = 0; i < names.length ; i++)
            score += ruleRepository.findPointsByComponentAndNameAndMinBound(NEGATIVE_COMPONENT, names[i], extracted_scores[i]);

        return score;
    }

    public int getPositiveScore(JSONObject jsonObject) {
        final String FIBER_100G = "fiber_100g";
        final String PROTEINS_100G = "proteins_100g";
        final String POSITIVE_COMPONENT = "P";

        JSONObject nutriments = jsonObject.getJSONObject("product")
                .getJSONObject("nutriments");
        float fiber_100g = nutriments.getFloat("fiber_100g");
        float proteins_100g = nutriments.getFloat("proteins_100g");

        float[] extracted_scores = {fiber_100g, proteins_100g};
        String[] names = {FIBER_100G, PROTEINS_100G};
        int score = 0;
        for(int i = 0; i < names.length ; i++)
            score += ruleRepository.findPointsByComponentAndNameAndMinBound(POSITIVE_COMPONENT, names[i], extracted_scores[i]);

        return score;
    }

    public BasketRequestDto getBasketScore(BasketRequestDto basketRequestDto) {

        int sumNutritionScore = basketRequestDto.getBasketsDto().stream()
                            .map(basketDto -> basketDto.getQte() * this.getProduct(basketDto.getBarCode()).getNutritionScore())
                            .reduce(0, (subtotal, nutritionScore) -> subtotal + nutritionScore);

        int nbProducts = basketRequestDto.getBasketsDto().stream()
                        .map(basketDto -> basketDto.getQte())
                        .reduce(0, (subtotal, nbProduct) -> subtotal + nbProduct);

        float avgNutritionScore = sumNutritionScore / (nbProducts * 1.f);

        Optional<NutritionScore> basketNutritionScore = nutritionScoreRepository.findClasseAndColorByNutritionScore(avgNutritionScore);

        basketRequestDto.setNutritionScore(avgNutritionScore);
        basketRequestDto.setClasse(basketNutritionScore.get().getClasse());
        basketRequestDto.setColor(basketNutritionScore.get().getColor());

        return basketRequestDto;
    }
}


