package com.javapro.tp1.dal;

public interface ProductDAL {
    String getProductAsJSON(String bareCode);
}
