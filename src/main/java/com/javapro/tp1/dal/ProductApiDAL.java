package com.javapro.tp1.dal;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
@Component(value="ProductApiDAL")
public class ProductApiDAL implements  ProductDAL {

    private final RestTemplate restTemplate;
    private final String url = "https://fr.openfoodfacts.org/api/v0/produit/";

    @Override
    public String getProductAsJSON(String barCode) {
        return restTemplate.getForObject(url + barCode + ".json", String.class);
    }
}
