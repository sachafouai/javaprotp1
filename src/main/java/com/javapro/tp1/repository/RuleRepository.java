package com.javapro.tp1.repository;

import com.javapro.tp1.model.Rule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RuleRepository  extends JpaRepository<Rule, Long> {

    @Query(value =  "SELECT MAX(points) FROM rule where component = ?1 " +
                    "AND name = ?2 " +
                    "AND ?3 > min_bound", nativeQuery = true )
    int findPointsByComponentAndNameAndMinBound(String component, String name, float minBound);

}
