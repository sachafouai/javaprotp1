package com.javapro.tp1.repository;

import com.javapro.tp1.model.NutritionScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface NutritionScoreRepository  extends JpaRepository<NutritionScore, Long> {
    @Query(value =  "SELECT * FROM nutrition_score WHERE lower_bound <= ?1 AND  ?1 <= upper_bound",
            nativeQuery = true )
    Optional<NutritionScore> findClasseAndColorByNutritionScore(float nutritionScore);
}
