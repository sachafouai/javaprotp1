package com.javapro.tp1.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Rule {

    @Id
    private Long id;

    private String name;
    private int points;
    private float min_bound;
    private String component;


}
