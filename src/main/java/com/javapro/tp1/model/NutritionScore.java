package com.javapro.tp1.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class NutritionScore {
    @Id
    private Long id;
    private String classe;
    private int lower_bound;
    private int  upper_bound;
    private String color;
}
