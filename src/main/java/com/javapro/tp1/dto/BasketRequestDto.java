package com.javapro.tp1.dto;

import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class BasketRequestDto {

    private String email;

    @Nullable
    private float nutritionScore;
    @Nullable
    private String classe;
    @Nullable
    private String color;

    private List<BasketDto> basketsDto;
}
