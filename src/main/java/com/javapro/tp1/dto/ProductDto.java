package com.javapro.tp1.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductDto implements Serializable {
        private String id; // barCode
        private String barCode;
        private String name;
        private int nutritionScore;
        private String classe;
        private String color;



        // barCode = product->_id
        // product, ecoscore_data, agribalyse,  name_fr

}
