package com.javapro.tp1.bean;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestTemplateCreator {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}



