package com.javapro.tp1.controller;

import com.javapro.tp1.dto.BasketRequestDto;
import com.javapro.tp1.dto.ProductDto;
import com.javapro.tp1.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/")
@AllArgsConstructor
public class MainController {

    final private ProductService productService;

    @GetMapping("productInfo/{barCode}")
    ResponseEntity<ProductDto> getProduct(@PathVariable("barCode") String barCode)
    {
        ProductDto productDto = productService.getProduct(barCode);
        return ResponseEntity.status(HttpStatus.OK)
                .body(productDto);
    }

    @PostMapping("basket")
    ResponseEntity<BasketRequestDto> getBasketScore(@RequestBody BasketRequestDto basketRequestDto)
    {

        BasketRequestDto basketDto = productService.getBasketScore(basketRequestDto);
        return ResponseEntity.status(HttpStatus.OK)
                .body(basketDto);
    }
}
