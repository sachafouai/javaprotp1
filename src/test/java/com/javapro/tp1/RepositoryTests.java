package com.javapro.tp1;

import com.javapro.tp1.model.NutritionScore;
import com.javapro.tp1.repository.NutritionScoreRepository;
import com.javapro.tp1.repository.RuleRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@DataJpaTest
public class RepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private RuleRepository ruleRepository;

    @Autowired
    private NutritionScoreRepository nutritionScoreRepository;

    @Test
    @DisplayName("Test du repository RuleRepository")
    void ruleRepositoryTest() {
        int scoreNegative =
                ruleRepository
                        .findPointsByComponentAndNameAndMinBound("N", "salt_100g", 180);

        int scorePositive =
                ruleRepository
                        .findPointsByComponentAndNameAndMinBound("P", "proteins_100g", 4.7f);

        assertEquals(1, scoreNegative);
        assertEquals(2, scorePositive);
    }

    @Test
    @DisplayName("Test du repository NutritionScoreRepository")
    void NutritionScoreRepositoryTest() {
        Optional<NutritionScore>  nutritionScoreList = nutritionScoreRepository.findClasseAndColorByNutritionScore(20);
        assertEquals(nutritionScoreList.get().getClasse(), "Degueu");
        assertEquals(nutritionScoreList.get().getColor(), "green");
    }


}
