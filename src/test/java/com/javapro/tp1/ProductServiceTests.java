package com.javapro.tp1;

import com.javapro.tp1.dal.ProductDAL;
import com.javapro.tp1.model.NutritionScore;
import com.javapro.tp1.repository.NutritionScoreRepository;
import com.javapro.tp1.repository.RuleRepository;
import com.javapro.tp1.service.ProductService;
import org.json.JSONObject;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ProductServiceTests {

	@Mock
	private RuleRepository ruleRepository;

	@Mock
	private NutritionScoreRepository nutritionScoreRepository;

	@Mock
	private ProductDAL productDAL;

	@Mock
	private RestTemplate restTemplate;

	@InjectMocks
	private ProductService productService;

	@Test
	@DisplayName("Le score de la composante negative est correct")
	void testNegativeComponentScore() {

		String jsonData = getJSONObject_t();
		JSONObject jsonObject = new JSONObject(jsonData);
		when(ruleRepository.findPointsByComponentAndNameAndMinBound(anyString(),anyString(), anyFloat()))
				.thenReturn(1);

		assertEquals(4, productService.getNegativeScore(jsonObject));
	}

	@Test
	@DisplayName("Le score de la composante positive est correct")
	void testPositiveComponentScore() {
		String jsonData = getJSONObject_t();
		JSONObject jsonObject = new JSONObject(jsonData);
		when(ruleRepository.findPointsByComponentAndNameAndMinBound(anyString(),anyString(), anyFloat()))
				.thenReturn(1);

		assertEquals(2, productService.getPositiveScore(jsonObject));
	}

	@Test
	@DisplayName("Le score nutritionnel est correct")
	void testNutritionalScore() {
		String barCode = "7622210449283";
		String jsonData = getJSONObject_t();

		when(productDAL.getProductAsJSON(anyString()))
				.thenReturn(jsonData);

		when(nutritionScoreRepository.findClasseAndColorByNutritionScore(anyFloat()))
				.thenReturn(Optional.of(new NutritionScore(2L, "Bon", 0, 2, "light green")));

		assertEquals("Bon", productService.getProduct(barCode).getClasse());
		assertEquals("light green", productService.getProduct(barCode).getColor());

	}

	private String getJSONObject_t() {
		String jsonData = "{" +
				"\"code\": \"7622210449283\"," +
				"\"product\": {" +
					"\"_id\": \"7622210449283\"," +
					"\"ecoscore_data\": {" +
						"\"agribalyse\": {" +
							"\"name_fr\": \"Biscuit sec chocolaté, préemballé\"," +
						"}" +
					"}," +
					"\"nutriments\": {" +
							"\"energy_100g\": 336," +
							"\"sugars_100g\": 4.6," +
							"\"salt_100g\": 91," +
							"\"fiber_100g\": 0.9," +
							"\"proteins_100g\": 1.6" +
					"}," +
					"\"nutriscore_data\": {" +
						"\"saturated_fat\": 1.5" +
						"}" +
					"}" +
				"}" +
				"}";

		return jsonData;
	}
}
