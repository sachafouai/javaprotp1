package com.javapro.tp1;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.javapro.tp1.dto.BasketDto;
import com.javapro.tp1.dto.BasketRequestDto;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ControllerTests {

    @Autowired
    private MockMvc mvc;

    @Test
    @DisplayName("Les informations du produit reçues sont correctes")
    void getProductInfoTest() throws Exception {

        mvc.perform(get("/api/productInfo/7622210449283"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.barCode", is("7622210449283")))
                .andExpect(jsonPath("$.nutritionScore", is(10)))
                .andExpect(jsonPath("$.classe", is("Mangeable")))
                .andExpect(jsonPath("$.color", is("yellow")));
    }

    @Test
    @DisplayName("le résultat du basket reçue est correcte")
    void addBasketTest() throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        List<BasketDto> baskets = Arrays.asList(
                                                BasketDto.builder()
                                                        .barCode("7622210449283")
                                                        .qte(11).build(),
                                                BasketDto.builder()
                                                        .barCode("7622210449283")
                                                        .qte(2).build());

        BasketRequestDto basketRequestDto = BasketRequestDto.builder()
                                                            .email("test@gmail.com")
                                                            .basketsDto(baskets).build();

        mvc.perform(post("/api/basket")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(basketRequestDto)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.nutritionScore", is(10.0)))
                .andExpect(jsonPath("$.classe", is("Mangeable")))
                .andExpect(jsonPath("$.color", is("yellow")));

    }




}
